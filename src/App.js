import { useState } from 'react';
import './App.css';

function App() {
  const [counter, setCounter] = useState(0);
  return (
    <div className="App">
      <header className="App-header">
        Hello Counter
      </header>
      <div className="content">
        <div className="row">
          <button onClick={() => setCounter(counter - 1)}>Decrease Counter</button>
            {counter}
          <button onClick={() => setCounter(counter + 1)}>Increase Counter</button>
        </div>
        <div className="row">
          <button onClick={() => setCounter(0)}>Reset Counter</button>
        </div>
      </div>
    </div>
  );
}

export default App;
